# Block title class
Provides a possibility to add a headline class (h1 - h6) to the block title.

## HTML in Block
```twig
<h2{{ title_attributes }}>{{ label }}</h2>
```
